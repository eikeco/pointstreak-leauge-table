<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Pointstreak API</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			//ini_set('display_errors', 'On');
			function file_get_contents_curl($url) {
				$headers = array(
					"apikey: [YOUR_API_KEY_HERE]"
				);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
				$data = curl_exec($ch);
				$retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);
				if ($retcode == 200) {
					return $data;
				} else {
					return "The result was " . $retcode;
				}
			}
			$response = file_get_contents_curl('https://api.pointstreak.com/hockey/season/standings/15636/json');
			$jsonify = json_decode($response);
			$teams = $jsonify->standings->team;
		?>

		<h1>EIHL 2016/17 Standings</h1>

		<table>
			<thead>
				<tr>
					<th>Team</th>
					<th>Games Played</th>
					<th>Wins</th>
					<th>Losses</th>
					<th>Points</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($teams as $team) { ?>
					<tr>
						<td>
							<?php echo $team->{'teamname'}; ?>
						</td>
						<td>
							<?php echo $team->{'gp'}; ?>
						</td>
						<td>
							<?php echo $team->{'wins'}; ?>
						</td>
						<td>
							<?php echo $team->{'losses'}; ?>
						</td>
						<td>
							<?php echo $team->{'points'}; ?>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</body>
</html>
